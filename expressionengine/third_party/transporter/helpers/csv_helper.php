<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('output_csv')) {
    function output_csv($array, $filename = "", $delimiter = ",", $eol = "\r\n") {
        if (empty($filename)) {
            //###   Create new filename   ###
            $filename = "ee-db-csv-export-".date(Ymd)."-".date(His).".csv";

        } else {
            //###   Check Extension is correct   ###
            $pos = strrpos($filename, ".");
            if ($pos > 0)
                $filename = substr($filename, 0, $pos);
            $filename .= ".csv";
        }

        //###   Create File Headers   ###
        header('Content-Encoding: UTF-8');
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachement; filename="' . $filename . '"');

        //ob_start();
        //###   Keep up to 12MB in memory, if becomes bigger write to temp file   ###
        $fileHandle = fopen('php://temp/maxmemory:'. (12*1024*1024), 'w+b');
        if ($fileHandle) {
            $loopNum = 0;
            foreach ($array as $line) {
                $loopNum++;
                if (fputcsv($fileHandle, $line, $delimiter)) {
                    if ("\n" != $eol && fseek($fileHandle, -1, SEEK_CUR) === 0) {
                        fwrite($fileHandle, $eol);
                    } else {
                        //###   Move Pointer to end of file   ###
                        if (fseek($fileHandle, 0, SEEK_END) === -1) {
                            // handle error
                        }
                    }
                } else {
                    // handle error
                    show_error("Cannot write line $loopNum: ".substr(implode(",",$line),0, 150));
                }
            }
            rewind($fileHandle);
            $content = stream_get_contents($fileHandle);
            fclose($fileHandle);
            //$content = ob_get_contents();
            //ob_end_clean();
            print_r($content);
            return $content;

        } else {
            //###   File Open Failed   ###
            show_error("Cannot open php://temp");
            return false;
        }
    }//###   End of output_csv function */
}


if ( !function_exists('build_csv_array')) {
    function build_csv_array($data, $headerRow = true) {
        $outputArray = array();

        //###   Detect if QueryObject or Array   ###
        if (is_object($data) && method_exists($data, 'list_fields')) {
            //###   DB Query Result passed   ###
            if ($headerRow) {
                foreach ($data->list_fields() as $name)
                    $fields[] = $name;

                $outputArray[] = $fields;
            }

            foreach ($data->result_array() as $row)
                $outputArray[] = array_values($row);

        } else if (is_array($data)) {
            //###   Array passed   ###
            if ($headerRow) {
                $outputArray[] = array_keys($data[0]);
            }

            foreach ($data as $row)
                $outputArray[] = array_values($row);

        } else {
            //###  Junk passed!   ###
            show_error('Invalid data passed to build_csv_array');
        }

        return $outputArray;

    }//###   End of build_csv_array function
}


/* End of file csv_helper.php */
/* Location: ./helpers/csv_helper.php */