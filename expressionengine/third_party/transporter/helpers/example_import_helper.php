<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$filename = "";

$update = false;

if ( !function_exists('exampleMap')) {
    function exampleMap() {
return array(
    0   =>  array(  "original"      => "entry_id",
                    "field_label"   => "Entry ID",
                    "field_name"    => "entry_id",
                    "field_ref"     => "entry_id",
                    "field_type"    => "ee"),
    1   =>  array(  "original"      => "channel_id",
                    "field_label"   => "Channel ID",
                    "field_name"    => "channel_id",
                    "field_ref"     => "channel_id",
                    "field_type"    => "ee"),
    2   =>  array(  "original"      => "author_id",
                    "field_label"   => "Author ID",
                    "field_name"    => "author_id",
                    "field_ref"     => "author_id",
                    "field_type"    => "ee"),
    3   =>  array(  "original"      => "title",
                    "field_label"   => "Entry Title",
                    "field_name"    => "title",
                    "field_ref"     => "title",
                    "field_type"    => "ee"),
    4   =>  array(  "original"      => "url_title",
                    "field_label"   => "URL Title",
                    "field_name"    => "url_title",
                    "field_ref"     => "url_title",
                    "field_type"    => "ee"),
    5   =>  array(  "original"      => "status",
                    "field_label"   => "Status",
                    "field_name"    => "status",
                    "field_ref"     => "status",
                    "field_type"    => "ee"),
    6   =>  array(  "original"      => "entry_date",
                    "field_label"   => "Entry Date",
                    "field_name"    => "entry_date",
                    "field_ref"     => "entry_date",
                    "field_type"    => "ee"),
    7   =>  array(  "original"      => "expiration_date",
                    "field_label"   => "Expiration Date",
                    "field_name"    => "expiration_date",
                    "field_ref"     => "expiration_date",
                    "field_type"    => "ee"),
    8   =>  array(  "original"      => "site_id",
                    "field_label"   => "Site ID",
                    "field_name"    => "site_id",
                    "field_ref"     => "site_id",
                    "field_type"    => "ee"),

    11  =>  array(  "original"      => "content-text",
                    "field_label"   => "Product Description",
                    "field_name"    => "description",
                    "field_ref"     => "field_id_16",
                    "field_type"    => "Textarea (Rich Text)"),
    20  =>  array(  "original"      => "featured-products",
                    "field_label"   => "Related Products",
                    "field_name"    => "related-products",
                    "field_ref"     => "field_id_19",
                    "field_type"    => "playa"),

    40  =>  array(  "original"      => "image-title",
                    "field_label"   => "Images",
                    "field_name"    => "product-images",
                    "field_ref"     => "field_id_17",
                    "field_type"    => "matrix",
                    "item_name"     => "image-title"),
    41  =>  array(  "original"      => "image",
                    "field_label"   => "Images",
                    "field_name"    => "product-images",
                    "field_ref"     => "field_id_17",
                    "field_type"    => "matrix",
                    "item_name"     => "image"),
    42  =>  array(  "original"      => "alt-text",
                    "field_label"   => "Images",
                    "field_name"    => "product-images",
                    "field_ref"     => "field_id_17",
                    "field_type"    => "matrix",
                    "item_name"     => "alt-text"),
    43  =>  array(  "original"      => "video",
                    "field_label"   => "Images",
                    "field_name"    => "product-images",
                    "field_ref"     => "field_id_17",
                    "field_type"    => "matrix",
                    "item_name"     => "video"),
    44  =>  array(  "original"      => "download",
                    "field_label"   => "Images",
                    "field_name"    => "product-images",
                    "field_ref"     => "field_id_17",
                    "field_type"    => "matrix",
                    "item_name"     => "download"),

    49  =>  array(  "original"      => "sku",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name"     => "stock:sku",
                    "secondary"     => true),


    60  =>  array(  "original"      => "weight",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name" => "weight"),

    67  =>  array(  "original"      => "unique_views",
                    "field_label"   => "",
                    "field_name"    => "view_count_one",
                    "field_ref"     => "view_count_one",
                    "field_type"    => "ee",
                    "item_name"     => ""),

    74  =>  array(  "original"      => "price_GBP",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name"     => "regular_price"),
    75  =>  array(  "original"      => "sale_price_GBP",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name"     => "sale_price"),
    77  =>  array(  "original"      => "variants_sku",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name"     => "stock:sku"),
    78  =>  array(  "original"      => "variants_custom-30",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name"     => "modifier:Colour"),
    79  =>  array(  "original"      => "variants_custom-29",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name"     => "modifier:Size"),
    83  =>  array(  "original"      => "variants_stock",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name"     => "stock:stock_level"),
    86  =>  array(  "original"      => "dimensions_width",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name"     => "dimension_w"),
    87  =>  array(  "original"      => "dimensions_height",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name"     => "dimension_h"),
    88  =>  array(  "original"      => "dimensions_depth",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name"     => "dimension_l"),
    89  =>  array(  "original"      => "delivery_cost_GBP",
                    "field_label"   => "Product Details",
                    "field_name"    => "details",
                    "field_ref"     => "field_id_20",
                    "field_type"    => "store",
                    "item_name"     => "handling")
    );
}
}

if ( !function_exists('exampleFileMap')) {
    function exampleFileMap() {
        return array(
            2   =>  1,
            3   =>  1,
            4   =>  1
        );
    }
}

if ( !function_exists('exampleAuthorMap')) {
    function exampleAuthorMap() {
        return array(
            4   =>  1
        );
    }
}


$OLDmapping = array(
    "entry_id" =>   array(          index => 0,
                                    field_label => "Entry ID",
                                    field_name => "entry_id",
                                    field_ref => "entry_id",
                                    field_type => "ee"),
    "channel_id" =>   array(        index => 0,
                                    field_label => "Channel ID",
                                    field_name => "channel_id",
                                    field_ref => "channel_id",
                                    field_type => "ee"),
    "author_id" =>   array(         index => 0,
                                    field_label => "Author ID",
                                    field_name => "author_id",
                                    field_ref => "author_id",
                                    field_type => "ee"),
    "title" =>   array(             index => 0,
                                    field_label => "Entry Title",
                                    field_name => "title",
                                    field_ref => "title",
                                    field_type => "ee"),
    "url_title" =>   array(         index => 0,
                                    field_label => "URL Title",
                                    field_name => "url_title",
                                    field_ref => "url_title",
                                    field_type => "ee"),
    "status" =>   array(            index => 0,
                                    field_label => "Status",
                                    field_name => "status",
                                    field_ref => "status",
                                    field_type => "ee"),
    "entry_date" =>   array(        index => 0,
                                    field_label => "Entry Date",
                                    field_name => "entry_date",
                                    field_ref => "entry_date",
                                    field_type => "ee"),
    "expiration_date" =>   array(   index => 0,
                                    field_label => "Expiration Date",
                                    field_name => "expiration_date",
                                    field_ref => "expiration_date",
                                    field_type => "ee"),
    "site_id" =>   array(           index => 0,
                                    field_label => "Site ID",
                                    field_name => "site_id",
                                    field_ref => "site_id",
                                    field_type => "ee"),
    "alternative-title" => "",
    "lead-in" => "",
    "content-text" =>   array(      index => 0,
                                    field_label => "Product Description",
                                    field_name => "description",
                                    field_ref => "field_id_16",
                                    field_type => "Textarea (Rich Text)"),
    "additional-text" => "",
    "meta-title" => "",
    "meta-description" => "",
    "meta-keywords" => "",
    "images" => "images",
    "left-panels" => "",
    "right-panels" => "",
    "slideshow" => "",
    "featured-products" =>   array( index => 0,
                                    field_label => "Related Products",
                                    field_name => "related-products",
                                    field_ref => "field_id_19",
                                    field_type => "playa"),
    "snippet" => "",
    "size-guide" => "",
    "expert-advice" => "",
    "panel-template" => "",
    "product-list" => "",
    "available-sizes" => "",
    "available-colours" => "",
    "hp-img" => "",
    "site-wide-img" => "",
    "basket-img" => "",
    "account-img" => "",
    "checkout-img" => "",
    "link-description" => "",
    "link-url" => "",
    "hide-navigation" => "",
    "rs_cat_list" => "",
    "slider_image" => "",
    "slider_url" => "",
    "slider_text" => "",
    "image-title" => array( index => 0,
                            field_label => "",
                            field_name => "product-images",
                            field_ref => "field_id_17",
                            item_name => "image-title",
                            field_type => "matrix"),
    "image" =>  array(      index => 0,
                            field_label => "",
                            field_name => "product-images",
                            field_ref => "field_id_17",
                            item_name => "image",
                            field_type => "matrix"),
    "alt-text" =>   array(  index => 0,
                            field_label => "",
                            field_name => "product-images",
                            field_ref => "field_id_17",
                            item_name => "alt-text",
                            field_type => "matrix"),
    "video" =>   array(     index => 0,
                            field_label => "",
                            field_name => "product-images",
                            field_ref => "field_id_17",
                            item_name => "video",
                            field_type => "matrix"),
    "download" =>   array(  index => 0,
                            field_label => "",
                            field_name => "product-images",
                            field_ref => "field_id_17",
                            item_name => "download",
                            field_type => "matrix"),
    "download-title" => "",
    "slideshow-image" => "",
    "image-link" => "",
    "id" => "",
    "sku" =>   array(   index => 0,
                        field_label => "",
                        field_name => "details",
                        field_ref => "field_id_20",
                        item_name => "image-title",
                        field_type => "store"),
    "price" => "",
    "sale_price" => "",
    "cost_price" => "",
    "rrp_price" => "",
    "discount" => "",
    "variants" => "",
    "stock_override" => "",
    "stock_level" => "",
    "stock_warning" => "",
    "stock_warning_override" => "",
    "weight"             =>   array(index => 0,
                                    field_label => "Weight",
                                    field_name => "details",
                                    field_ref => "field_id_20",
                                    item_name => "weight",
                                    field_type => "store"),
    "dimensions" => "",
    "delivery_cost" => "",
    "delivery_exclusion" => "",
    "tax_inclusion" => "",
    "tax_rate" => "",
    "tax_override" => "",
    "unique_views" =>   array(      index => 0,
                                    field_label => "Site ID",
                                    field_name => "view_count_one",
                                    field_ref => "view_count_one",
                                    field_type => "ee"),
    "total_cart_adds" => "",
    "total_sales" => "",
    "source_id" => "",
    "deferred_payment" => "",
    "barcode" => "",
    "reference" => "",
    "price_GBP"         =>   array( index => 0,
                                    field_label => "",
                                    field_name => "details",
                                    field_ref => "field_id_20",
                                    item_name => "regular_price",
                                    field_type => "store"),
    "sale_price_GBP"    =>   array( index => 0,
                                    field_label => "",
                                    field_name => "details",
                                    field_ref => "field_id_20",
                                    item_name => "sale_price",
                                    field_type => "store"),
    "discount_GBP" => "",
    "variants_sku"       =>   array(index => 0,
                                    field_label => "SKU",
                                    field_name => "details",
                                    field_ref => "field_id_20",
                                    item_name => "sku",
                                    field_type => "store"),
    "variants_custom-30" =>   array(index => 0,
                                    field_label => "Colour",
                                    field_name => "details",
                                    field_ref => "field_id_20",
                                    item_name => "modifiers",
                                    field_type => "store"),
    "variants_custom-29" =>   array(index => 0,
                                    field_label => "Size",
                                    field_name => "details",
                                    field_ref => "field_id_20",
                                    item_name => "modifiers",
                                    field_type => "store"),
    "variants_price" => "",
    "variants_sale" => "",
    "variants_discount" => "",
    "variants_stock"     =>   array(index => 0,
                                    field_label => "Stock Level",
                                    field_name => "details",
                                    field_ref => "field_id_20",
                                    item_name => "stock_level",
                                    field_type => "store"),
    "variants_weight" => "",
    "variants_delivery" => "",
    "dimensions_width"   =>   array(index => 0,
                                    field_label => "Width",
                                    field_name => "details",
                                    field_ref => "field_id_20",
                                    item_name => "dimension_w",
                                    field_type => "store"),
    "dimensions_height"   =>  array(index => 0,
                                    field_label => "Width",
                                    field_name => "details",
                                    field_ref => "field_id_20",
                                    item_name => "dimension_h",
                                    field_type => "store"),
    "dimensions_depth"   =>   array(index => 0,
                                    field_label => "Width",
                                    field_name => "details",
                                    field_ref => "field_id_20",
                                    item_name => "dimension_l",
                                    field_type => "store"),
    "delivery_cost_GBP"   =>  array(index => 0,
                                    field_label => "Width",
                                    field_name => "details",
                                    field_ref => "field_id_20",
                                    item_name => "handling",
                                    field_type => "store"),
    "rrp_price_GBP" => ""

    );

if ( !function_exists('createNewEntries')) {
    function createNewEntries($channelID, $rowData) {
        $this->EE->load->library('api');
        $this->EE->api->instantiate('channel_entries');
        $this->EE->api->instantiate('channel_fields');
        $this->EE->api_channel_fields->fetch_custom_channel_fields();

        $entryIDs = array();
        foreach($rowData as $key => $data) {

            $importEntry = array();
//            foreach($data as $key => $data) {
//            $this->mapping;

            if ($this->EE->api_channel_entries->submit_new_entry($channelID, $data) === FALSE) {
                //Log Error

            } else {
                $entryIDs[$key] = $this->EE->api_channel_entries->entry_id;
            }
        }

        //###   Playa compatability - create lookup for old and new entry_ids   ###


        return $entryIDs;
    }//###   End of createNewEntries function
}


if ( !function_exists('updateEntries')) {
    function updateEntries($channelID, $rowData) {
        $this->EE->load->library('api');
        $this->EE->api->instantiate('channel_entries');
        $this->EE->api->instantiate('channel_fields');
        $this->EE->api_channel_fields->fetch_custom_channel_fields();

        $entryIDs = array();
        foreach($rowData as $key => $data) {
            $entryID = false;
            if ($entryID) {
                //###   Update Entry   ###

            } else {
                //###   Create Entry   ###
                if ($this->EE->api_channel_entries->submit_new_entry($channelID, $data) === FALSE) {
                    //Log Error

                } else {
                    $entryIDs[$key] = $this->EE->api_channel_entries->entry_id;
                }
            }
        }

    }//###   End of updateEntries function
}


