/*###   Module Control Panel JS   ###*/
$(document).ready(function() {
    //#####################################################################
    //###############        Control Panel - GENERAL        ###############
    //#####################################################################
    if ($("#email-list").length) {
        $("#email-list").dataTable({
            "aaSorting": [[ 1, "asc" ]],
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
            "sPaginationType": "full_numbers"
        });
    }
});