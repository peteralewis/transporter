<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang = array(

    //###   Required for Extension page
    "transporter_module_name"           => "Transporter: Entry Import/Export",
    "transporter_module_description"	=> "Full entry data import and export utility of Entries with support for Matrix, Playa, Exp:resso Store products and custom tables",

    "unauthorized_access"		=> "You are not authorised to view this page",

    "crumb_home"                => "Transporter: Entry Import/Export",
    "crumb_export"              => "Export",
    "settings_butn"             => "Settings",
    "home_butn"                 => "Home",
    "export_butn"               => "Export",
    "import_butn"               => "Import",
    "settings_title"            => "Transporter Import/Export Settings",
    "settings_desc"             => "Adjust the settings for Transporter Import/Export Utility",
    "settings_save"             => "Save Settings",
    "settings_save_success"     => "Settings Saved",
    "settings_save_failure"     => "Failed to save Settings!",
    "export_title"              => "Export",
    "export_run_butn"           => "Export NOW!",
    "msg_xid_failure"           => "Form submission failed the Security check, please try again",
    "msg_export_no_channel"     => "Form validation issue: No Channel(s) selected - so no entries to export!",
    "msg_export_successful"     => "Export successful!",
    "msg_export_failed"         => "Export failed - please see log file",
    "msg_export_no_filename"    => "Export failed - No filename specified",
    "label_channels"            => "Select channels to be included in this export",
    "label_tables"              => "Select <strong>additional</strong> tables to be included in this export",
    "label_include_matrix"      => "Include Matrix fields?",
    "label_export_format"       => "Select the format for export",
    "option_export_format_csv"  => "CSV",
    "option_export_format_xml"  => "XML",
    "label_filename"            => "Enter a filename for the export (<strong>without</strong> extension)",
    "label_delimiter"           => "Enter a CSV delimiter to separate column data",
    "label_newline"             => "Enter newline code to include in the output",

// END
''=>''
);