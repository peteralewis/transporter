<?php if (!defined('EXT')) exit('Invalid file request');

/*====================================================================================================
  Copyright (c) 2013 Peter Lewis http://www.peteralewis.com
  ====================================================================================================*/
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', true);

class import {
    var $EE, $site_id;
    var $third_parties;
    var $importMap;
    var $uploadFolderMap;
    var $entryIDLookup;

    //###   PHP5 Constructor   ###
    function __construct() {
        $this->EE = & get_instance();
        $this->site_id = $this->EE->config->item('site_id');
    }//###   End of __construct function


    function process($preview = true, $uploadField = "upload-filename") {
       $config = array();
//TEMP
$this->EE->load->helper('example_import');

        //###   Set file upload settings   ###
        $this->CI =& get_instance();
        $config['allowed_types'] = 'txt|csv|xls|xlsx';
        $config['overwrite'] = true;
        if (!empty($this->EE->config->item['cache_path'])) {
//TO DO - Check Cache path ends in "/"
            $config['upload_path'] = $this->EE->config->item['cache_path'] . "/";
        } else {
            $config['upload_path'] = APPPATH . "cache/transporter/";
        }
        $config['file_name'] = "import-data.txt";


		//###   Load CI File Helper and check temp path   ###
		$this->EE->load->helper('file');
        $folderInfo = get_dir_file_info($config['upload_path']);
        if (empty($folderInfo)) {
            //###   Create the upload folder
            if (!mkdir($config['upload_path'])) {

//ERROR - unable to create folder
            }
        }

        //###   Load CI Upload Library and upload!   ###
        $this->CI->load->library('upload', $config);
        if ($this->CI->upload->do_upload($uploadField)) {
            //###   Successfully uploaded   ###

            $this->EE->load->helper('csv_helper');

            $fileInfo = $this->CI->upload->data();
//Mapping current hard-coded!
            $this->importMap = exampleMap();
            $this->uploadFolderMap = exampleFileMap();

            $rtnVal = $this->parse_csv_file($fileInfo["full_path"], 2, 1, $preview);
            if (!empty($rtnVal) && is_array($rtnVal)) {

//echo "<pre>";var_dump($rtnVal);echo "</pre>";
            } else {
echo "<pre>";var_dump($rtnVal);echo "</pre>";
echo "<pre>";var_dump("FAILED");echo "</pre>";
            }

//echo "<pre>";var_dump($mappedData);echo "</pre>";

            if ($preview) {
                //###   Preview, so only get first few rows   ###

            //} else {

            //    $rtnVal = process_import($mappedData);
            }

            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                //###   Ensure it's an Ajax request   ###

            } else {
                //###   Non-Ajax   ###

            }


        //return array as JSON object

        } else {
            $error = $this->CI->upload->display_errors();
echo "<pre>";var_dump("Import Lib line 93");echo "</pre>";
echo "<pre>";var_dump($error);echo "</pre>";
$fileInfo = $this->CI->upload->data();
//$fileInfo["full_path"]
//$fileInfo["client_name"]//File trying to upload
//ERROR
        }
    }//###   End of process function


    private function parse_csv_file($file, $fixedChannelID, $authorReset = false, $preview = false, $headerRow = true, $forceAdd = true) {
        $content = false;
        $fileHandle = fopen($file, 'r');
        $content = array();
        $counter = 0;
        $columnCheck = false;
        $entryCol = false;
        $channelCol = false;
        $addedEntries = array();
        $updateLookup = array();

        $this->third_party_support();
/*        $matrixAdditions = array();
        $storeAdditions = array();
        $playaAdditions = array();*/

        //###   Load EE API   ###
        $this->EE->load->library('api');
        $this->EE->api->instantiate('channel_entries');
        $this->EE->api->instantiate('channel_fields');
        //$this->EE->api_channel_fields->fetch_custom_channel_fields();
        //$this->EE->api_channel_fields->settings = array();




        if ($headerRow)
            $columnNames = fgetcsv($fileHandle, 4096);

        while (($row = fgetcsv($fileHandle, 4096)) != false) {

            //###   Only add row if not empty   ###
            if (!empty($row[0])) {

                //###   Get EntryID field/column   ###
                if ($columnCheck === false) {
                    $columnCheck = true;
                    $entryCol = $this->recursive_array_search("entry_id", $this->importMap);

                    if (empty($fixedChannelID)) {
                        $channelCol = $this->recursive_array_search("channel_id", $this->importMap);
                    }
                }

                if ($preview) {
                    //###   Initial preview, so indexing unknown   ###
                    $counter++;
                    $content[] = $row;
                    if ($counter >= 5)
                        break;

                } else {
                    $importEntryData = array();
                    //###   Clear temp third-party arrays   ###
                    foreach($this->third_parties as $tpKey => $tpValue)
                        $this->third_parties[$tpKey]["rowData"] = array();

                    /*$matrixEntryData = array();
                    $playaEntryData = array();
                    $storeEntryData = array(); */
                    //###   Manipulate Data and create import array with values from CSV   ###
                    foreach ($this->importMap as $ref => $fieldDetails) {
                        if (!empty($row[$ref])) {

                            if ($rtnVal = $this->third_party_mapping($row, $ref)) {
                                if ($rtnVal !== true) {
                                    //###   Has a "proper" value - so just save to field (nothing special)   ###
                                    $importEntryData[$this->importMap[$ref]["field_ref"]] = $rtnVal;
                                }

                            } else {
                                //###   Uses normal EE fields or basic fieldtypes   ###
                                if ($authorReset && $this->importMap[$ref]["field_ref"] == "author_id")
                                    $row[$ref] = $authorReset;
                                if ($authorReset && $this->importMap[$ref]["field_ref"] == "site_id")
                                    $row[$ref] = $this->site_id;

                                if ($this->importMap[$ref]["field_ref"] != "channel_id")
                                    $importEntryData[$this->importMap[$ref]["field_ref"]] = $row[$ref];
                            }
                        }
                    }//End of foreach


                    if (empty($fixedChannelID)) {
                        $channelID = $row[$channelCol];
                    } else {
                        $channelID = $fixedChannelID;
                    }

                    $new = false;
                    //###   Check if this entry has already been added from this import   ###
                    if (!array_key_exists($row[$entryCol], $addedEntries))
                        $new = true;
//TO DO: Run validation checks on input to ensure core fields exist and are valid


                    if ($new) {
                        //###   Add NEW Entry   ###
                        $this->EE->api_channel_fields->setup_entry_settings($channelID, $importEntryData);
                        if ($this->EE->api_channel_entries->submit_new_entry($channelID, $importEntryData) === FALSE) {
                            //###   ERROR!   ###
echo "<pre>";var_dump("EE Error");echo "</pre>";
//EXAMPLE:
/*array(2) {
  ["title"]=>
  string(13) "missing_title"
  ["url_title"]=>
  string(26) "unable_to_create_url_title"
}*/
echo "<pre>";var_dump($this->EE->api_channel_entries->errors);echo "</pre>";
//ERROR - Unable to add via API

                        } else {
                            //###   Yay! Saved basic Entry data - not out of the woods yet...   ###
                            //###   Save new entry_id and old entry_id together for additional row matching within this import   ###
                            $addedEntries[ $row[$entryCol] ] = $this->EE->api_channel_entries->entry_id;
//echo "<pre>";var_dump($this->EE->api_channel_entries->entry_id);echo "</pre>";
                            //NOTE: Will be empty "value" (new ID for key) if entryID NOT specified (aka new entry from import file)

                            //###   In case the Entry has to be edited, we need more details than just the entry_id due to crap API   ###
                            $updateLookup[$this->EE->api_channel_entries->entry_id] = array(
                                "channel_id" => $channelID,
                                "title"      => $importEntryData["title"]
                                );

                            //###   Third Party Fieldtype Support   ###
                            foreach($this->third_parties as $tpKey => $tpValue) {
                                if (!empty($this->third_parties[$tpKey]["rowData"])) {
                                    $this->third_parties[$tpKey]["entryData"][$row[$entryCol]][] = $this->third_parties[$tpKey]["rowData"];
                                }
                            }
                        }

                    } else {
                        //###   Update existing entry (that has been added in this import)   ###
                        //$this->EE->api_channel_entries->update_entry((int) $entry_id, (mixed) $data);

                        //###   Third Party Fieldtype Support   ###
                        foreach($this->third_parties as $tpKey => $tpValue) {
                            $this->third_parties[$tpKey]["entryData"][$row[$entryCol]][] = $this->third_parties[$tpKey]["rowData"];
                        }
//echo "<pre>";var_dump("Update");echo "</pre>";
//echo "<pre>";var_dump($importEntryData);echo "</pre>";
                    }
                }
            }
        }//End While
        fclose($fileHandle);

        $this->entryIDLookup = $addedEntries;
//echo "<pre>";var_dump($this->entryIDLookup);echo "</pre>";
        if ($preview) {
            return $content;

        } else {
//echo "<pre>";var_dump($this->third_parties);echo "</pre>";
            $this->third_party_entry_updates($updateLookup);
            return $addedEntries;
        }
    }//###   End of parse_csv_file function


    private function third_party_support() {
        $this->third_parties = array(
            "matrix"    =>  array("rowData" => array(), "entryData" => array()),
            "playa"     =>  array("rowData" => array(), "entryData" => array()),
            "store"     =>  array("rowData" => array(), "entryData" => array())
        );
        return;
    }//###   End of third_party_support function


    private function third_party_mapping($row, $column) {

        $rtnVal = false;
        $fieldType = $this->importMap[$column]["field_type"];

        if ($fieldType == "matrix") {
            $this->EE->load->model('export_model', 'export');
            $matrixCols = $this->EE->export->matrix_references();

            $matrixRowData = $this->change_upload_location($row[$column]);
            $this->third_parties[ $fieldType ]["rowData"][ $this->importMap[$column]["field_ref"] ][ $matrixCols[$this->importMap[$column]["item_name"]] ] = $matrixRowData;
            $rtnVal = true;

        } else if ($fieldType == "playa") {
            $this->third_parties[ $fieldType ]["rowData"][$this->importMap[$column]["field_ref"]] = $row[$column];
            $rtnVal = true;

        } else if ($fieldType == "store") {
            if (is_array($this->importMap[$column]["item_name"])) {
                $this->third_parties[ $fieldType ]["rowData"][ implode(":", $this->importMap[$column]["item_name"]) ] = $row[$column];

            } else {
                //###   Check for secondary mapping   ###
                if (empty($this->third_parties[ $fieldType ]["rowData"][$this->importMap[$column]["item_name"]])) {
                    $this->third_parties[ $fieldType ]["rowData"][$this->importMap[$column]["item_name"]] = $row[$column];
                    if ($this->importMap[$column]["secondary"] === true)
                        $this->third_parties[ $fieldType ]["rowData"]["secondary"] = true;
                } else {
                    if ($this->importMap[$column]["secondary"] !== true) {
                        $this->third_parties[ $fieldType ]["rowData"][$this->importMap[$column]["item_name"]] = $row[$column];
                        $this->third_parties[ $fieldType ]["rowData"]["secondary"] = false;
                    }
                }
            }
//echo "<pre>";var_dump($this->importMap[$column]);echo "</pre>";
//echo "<pre>";var_dump($row[$column]);echo "</pre>";
            $rtnVal = true;
//store_product_field[regular_price]
//store_product_field[modifiers][1][mod_name]
//store_product_field[modifiers][1][options][1][opt_name]//1st number is variant, 2nd is option row
//store_product_field[modifiers][1][options][2][opt_price_mod]
        }

        return $rtnVal;
    }//###   End of third_party_storage function


    private function third_party_entry_updates($updateLookup) {
        $this->EE->load->model('import_model', 'importModel');
        $this->EE->importModel->entryIDLookup = $this->entryIDLookup;

        //###   Loop through each third-party supported fieldtype   ###
        foreach($this->third_parties as $tpKey => $tpValue) {
            if ($tpKey == "matrix") {
                $rtnVal = $this->EE->importModel->add_matrix_entries($tpValue["entryData"], $updateLookup);

            } else if ($tpKey == "playa") {
                $rtnVal = $this->EE->importModel->add_playa_entries($tpValue["entryData"], $updateLookup);

            } else if ($tpKey == "store") {
                $rtnVal = $this->EE->importModel->add_store_entries($tpValue["entryData"]);

            }
        }
    }//###   End of third_party_entry_updates function


    private function change_upload_location($data) {
        if (!is_array($data) && !empty($this->uploadFolderMap)) {
            //###   Update folder (Upload Location) references   ###
            foreach($this->uploadFolderMap as $oldRef => $newRef) {
                $data = str_replace("{filedir_".$oldRef."}", "{filedir_".$newRef."}", $data);
            }
        }
        return $data;
    }//###   End of change_upload_location function


    function recursive_array_search($needle, $haystack) {
        foreach ($haystack as $key => $value) {
            $current_key = $key;
            if ($needle === $value OR (is_array($value) && $this->recursive_array_search($needle, $value) !== false)) {
                return $current_key;
            }
        }
        return false;
    }//###   End of recursive_array_search function

}//###   End of import Class