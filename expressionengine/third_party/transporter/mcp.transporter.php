<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
====================================================================================================
 Author: Peter Lewis
 http://www.peteralewis.com
====================================================================================================
 This file must be in a folder called transporter and placed in the ExpressionEngine installation third_party folder.
 version 		Version 1.0.0
 copyright 		Copyright (c) 2013 Peter Lewis
 Last Update    April 2013
----------------------------------------------------------------------------------------------------
 Change Log:

 v1.0.0  Initial version
====================================================================================================
*/

//###   Enable for testing   ###
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', true);

class Transporter_mcp {
    var $EE, $CI, $site_id, $themeFolder;
    var $class_name = "transporter";
    var $format, $filename;
    var $delimiter = ",";
    var $newline = "\r\n";
    var $uploadField = "upload-filename";

    //###   PHP5 Constructor   ###
    function __construct() {
        $this->EE =& get_instance();
        $this->site_id = $this->EE->config->item('site_id');
        //$this->themeFolder = $this->EE->config->item('theme_folder_url') . 'third_party/' . $this->class_name;
        $this->themeFolder = PATH_THIRD_THEMES . $this->class_name;

        $controls = array(
            lang('home_butn')       => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name,
            lang('export_butn')     => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name.AMP.'method=build_export',
            lang('import_butn')     => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name.AMP.'method=build_import',
            lang('settings_butn')   => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name.AMP.'method=settings');
        $this->EE->cp->set_right_nav($controls);
    }//###   End of __construct function

    //###   Main Control Panel page (Module homepage)   ###
    function index() {
        //$this->EE->cp->set_breadcrumb(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name, $this->EE->lang->line('crumb_home'));
        $this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('transporter_module_name'));
        $this->EE->cp->add_to_head('<link rel="stylesheet" type="text/css" media="screen" href="' . $this->themeFolder . '/css/control_panel.css" />');
        $this->EE->cp->load_package_js('jquery.dataTables.min');
        $this->EE->cp->load_package_js('control_panel');

/* EXAMPLE */
        $emailList = array();

        //###   Get List of emails   ###
/*        $sql = "SELECT * FROM ".$this->EE->db->dbprefix . "stock_email_alerts
		WHERE site_id = ".$this->site_id."
                AND sent = 0";
	$result = $this->EE->db->query($sql);
        if ($result->num_rows() > 0) {
            $emailList = $result->result_array();
        } */

        //###   Variables to send into view   ###
        $data = array(
                'emails'	=>  $emailList
        );
/* END EXAMPLE */
        return $this->EE->load->view('homepage', $data, TRUE);
    }

    function settings() {
        $this->EE->load->model('settings_model');
        $this->EE->cp->add_to_head('<link rel="stylesheet" type="text/css" media="screen" href="' . $this->themeFolder . '/css/control_panel.css" />');
        $this->EE->cp->set_breadcrumb(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name, $this->EE->lang->line('crumb_home'));
        $this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('settings_title'));

        $settings = $this->EE->settings->get_settings();

        //###   Variables to send into view   ###
        $data = array(
                'submitURL'	=>  BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name.AMP.'method=save_settings',
                'themeURL'	=>  $this->EE->config->item('theme_folder_url') . "third_party/".$this->class_name,
                'XID'		=>  $this->EE->functions->add_form_security_hash("{XID_HASH}"),
                'site_id' 	=>  $this->site_id,
                'settings'	=>  $settings
        );

        return $this->EE->load->view('settings', $data, TRUE);
    }

    function save_settings() {
        $this->EE->load->model('settings_model');
        if ( $this->EE->settings->set() ) {
            $this->EE->session->set_flashdata("message_success", lang('settings_save_success'));

            $redirectURL = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name;
            $this->EE->functions->redirect($redirectURL);
        } else {
            $this->EE->session->set_flashdata("message_failure", lang('settings_save_failure'));
        }
    }//###   End of save_settings function

    function build_export() {
        $this->EE->load->model('export_model', 'export');
        $this->EE->load->model('settings_model', 'settings');
        $this->EE->cp->add_to_head('<link rel="stylesheet" type="text/css" media="screen" href="' . $this->themeFolder . '/css/control_panel.css" />');
        $this->EE->cp->set_breadcrumb(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name, $this->EE->lang->line('crumb_home'));
        $this->EE->cp->set_breadcrumb(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name, $this->EE->lang->line('crumb_export'));
        $this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('export_title'));

        //echo "<pre>";var_dump($this->EE->export->get_tables());echo "</pre>";


        //###   Variables to send into view   ###
        $data = array(
                'submitURL'     =>  BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name.AMP.'method=start_export',
                'themeURL'      =>  $this->themeFolder . "/".$this->class_name . "/",
                'XID'           =>  $this->EE->functions->add_form_security_hash("{XID_HASH}"),
                'site_id'       =>  $this->site_id,
                'settings'      =>  $this->EE->settings->get(),
                'channels'      =>  $this->EE->export->get_channels(),
                'tables'        =>  $this->EE->export->get_tables(),
                'defaultNewline'    => $this->newline,
                'defaultDelimiter'  => $this->delimiter
        );

        return $this->EE->load->view('export_view', $data, TRUE);
    }//###   End of build_export function


    function customize_export() {
        //Allows user to select the individual fields to export

    }//###   End of customize_export function


    function start_export() {
//        $this->EE->load->library('logger');
		$redirectURL = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name.AMP.'method=build_export';
        $exportFramework = array();
        $saveID = false;

		$exportFramework["channels"] = $this->EE->input->post('channels');
		if (empty($exportFramework["channels"]) ) {
			$this->EE->session->set_flashdata("message_failure", lang('msg_export_no_channel'));
			$this->EE->functions->redirect($redirectURL);
			return;
		}
        if (is_array($exportFramework["channels"])) {
            foreach($exportFramework["channels"] as $ref => $channel)
                $exportFramework["channels"][$ref] = preg_replace("/[^0-9]/", '', $channel);
        } else {
            $exportFramework["channels"] = preg_replace("/[^0-9]/", '', $exportFramework["channels"]);
        }

		$exportFramework["additionalTables"] = $this->EE->input->post('tables');

        //###   If fields are Matrix   ###
        $param = $this->EE->input->post('matrix');
        if (strtolower($param) === "true" || strtolower($param) === "t" || strtolower($param) === "yes" || strtolower($param) === "y" || $param === "1")
            $exportFramework["includeMatrix"] = true;
        else
            $exportFramework["includeMatrix"] = false;

        //###   If fields contain serialized data   ###
        $param = $this->EE->input->post('serialized');
        if (strtolower($param) === "true" || strtolower($param) === "t" || strtolower($param) === "yes" || strtolower($param) === "y" || $param === "1")
            $exportFramework["hasSerializedData"] = true;
        else
            $exportFramework["hasSerializedData"] = false;

//TO DO - Allow user to select custom fields across all tables
        $exportFramework["customFields"] = "";

//TO DO - Allow user to specify output based on Status
        $exportFramework["statusOutput"] = "not closed";

//TO DO - Allow user to specify status output
        $exportFramework["limitOutput"] = 0;

//TO DO - Allow user to specify name (if saving)
        //###   Get name for export   ###
        $exportSettings["name"] = "";
        $param = $this->EE->input->post('export_name', "");
        if (!empty($param))
            $exportSettings["name"] = $this->EE->db->escape_str($this->EE->security->xss_clean($param));

        //###   Get notes for export   ###
        $exportSettings["notes"] = "";
        $param = $this->EE->input->post('export_notes', "");
        if (!empty($param))
            $exportSettings["notes"] = $this->EE->db->escape_str($this->EE->security->xss_clean($param));

        //###   Get format for exported file   ###
        $this->format = $this->EE->input->post('format', "csv");
        if ($this->format != "xml") {
            $this->format = "csv";

            //###   Get delimiter for CSV export
            $param = $this->EE->input->post('delimiter');
            if (!empty($param))
                $this->delimiter = $this->EE->db->escape_str($this->EE->security->xss_clean($param));

            //###   Get line delimiter for CSV export
            $param = $this->EE->input->post('newline', $this->newline);
            if (!empty($param))
                $this->newline = $this->EE->security->xss_clean($param);
        }
//TO DO - Add support for XML...?
        $exportSettings["format"] = $this->format;
        $exportSettings["delimiter"] = $this->delimiter;
        $exportSettings["newline"] = $this->newline;

        //###   Set the filename
        if (empty($this->filename)) {
            $this->filename = "ee-db-".$this->format."-export-".date(Ymd)."-".date(His).".".$this->format;
        } else {
            //###   Check Extension is correct   ###
            $pos = strrpos($this->filename, ".");
            if ($pos > 0)
                $this->filename = substr($this->filename, 0, $pos);
            $this->filename .= ".".$this->format;
        }
        $exportSettings["filename"] = $this->filename;

        $this->EE->load->model('export_model', 'exportMDL');
        $saveID = $this->EE->exportMDL->save_export($saveID, $exportFramework, $exportSettings);
        if ($saveID !== false) {
            $jobID = $this->EE->exportMDL->save_job($saveID, $exportSettings["filename"]);
            if ($jobID !== false) {




        //                jobID => $jobID,
          //              saveID => $saveID


//$exportFramework["limitOutput"] = 1000;
        $this->run_export_job($exportFramework, $jobID);

            }
        }
        exit;

    }//###   End of start_export function


    function run_export_job($framework, $jobID = false) {
        $totalRows = 0;

        if ($jobID === false) {
            //###   Get Job ID for export   ###
            $jobID = $this->EE->input->get_post('job_id', false);
        }
        $jobID = preg_replace("/[^0-9]/", '', $jobID);
//TO DO
//    Remove framework as parameter and check jobID as


        if ( !class_exists('exportMDL'))
            $this->EE->load->model('export_model', 'exportMDL');

//TO DO: Get Post JobID and SaveID, read DB and set $framework

        $exportQuery = $this->EE->exportMDL->get_entries($framework["channels"], $framework["includeMatrix"], $framework["additionalTables"], $framework["customFields"], $framework["statusOutput"], $framework["limitOutput"]);
//echo "<pre>row: ";var_dump($exportQuery);echo "</pre>";
        if ($exportQuery !== false) {
            $totalRows = $exportQuery->num_rows();
            $rtnVal = $this->process_export($exportQuery, $framework["hasSerializedData"]);
/* CAUSES ERRORS WITH SENT HEADERS
            if ($rtnVal === true)
                $this->EE->session->set_flashdata("message_success", lang('msg_export_successful'));
            else
                $this->EE->session->set_flashdata("message_failure", lang('msg_export_failed')); */

		} else {
			$this->EE->session->set_flashdata("message_failure", lang('msg_export_failed'));
//            $this->EE->logger->developer('Transporter Module: No filename specified on export, so no export generated!');
        }

        exit;

    }//###   End of run_export_job function


    //###   Import listing screen (create new import)   ###
    function import() {
        //MAY MOVE TO MAIN INDEX AND NOT SEPARATE!
    }


    //###   Configure a specific Import   ###
    function build_import() {
        $this->EE->load->model('import_model', 'import');
        $this->EE->load->model('settings_model', 'settings');
        $this->EE->cp->add_to_head('<link rel="stylesheet" type="text/css" media="screen" href="' . $this->themeFolder . '/css/control_panel.css" />');
        $this->EE->cp->set_breadcrumb(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name, $this->EE->lang->line('crumb_home'));
        $this->EE->cp->set_breadcrumb(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name, $this->EE->lang->line('crumb_import'));
        $this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('import_title'));

        //$viewData["form"] = $this->EE->functions->form_declaration($form_details);
        //###   Variables to send into view   ###
        $data = array(
                'submitURL'     =>  BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->class_name.AMP.'method=process_import',
                'themeURL'      =>  $this->themeFolder . "/".$this->class_name . "/",
                'XID'           =>  $this->EE->functions->add_form_security_hash("{XID_HASH}"),
                'site_id'       =>  $this->site_id,
                'settings'      =>  $this->EE->settings->get(),
                'stage'         =>  1,
                'uploadField'   =>  $this->uploadField
        );

        return $this->EE->load->view('import_file_view', $data, TRUE);

    }//###   End of build_import function


    //###   Reads in the specified file   ###
    function process_import() {
        $this->EE->load->library('import');

        //###   Check if preview or full import   ###
        $preview = $this->EE->input->get_post('preview');
        if ($preview !== true)
            $preview = false;

        $this->EE->import->process($preview, $this->uploadField);

        //exit;
    }//###   End of process_import function


    //###   Save the create import   ###
    function save_import() {
    }//###   End of save_import function


    //###   Preview and Map the import   ###
    function preview_import() {
    }//###   End of save_import function


    function run_import() {
        //$this->EE->load->helper('example_import');
        //$this->mapping;
    }//###   End of run_import function






    private function process_export($queryData, $hasSerializedData = false) {
        //###   Set PHP so it doesn't timeout!   ###
        set_time_limit(0);

        if($this->format == "xml") {
            $this->EE->load->dbutil();
            $config = array (
                    'root'      => 'root',
                    'element'   => 'element',
                    'newline'   => $this->newline,
                    'tab'       => '\t'
                );
            $exportData = $this->EE->dbutil->xml_from_result($queryData, $config);

            //###   Export to File   ###
            $this->EE->load->helper('download');
            force_download($this->filename, $exportData);
            return true;

        } else {
            //###   CSV   ###
            $this->EE->load->helper('csv_helper');
            $exportData = build_csv_array($queryData, true);
            $exportData = $this->clean_export_array($exportData, $hasSerializedData);
            $output = output_csv($exportData, $this->filename, $this->delimiter, $this->newline);

            //$this->EE->load->helper('download');
            //force_download($this->filename, $exportData);
            return true;
            //return $output;
        }
    }//###   End of process_export function


    private function clean_export_array($dataArray, $hasSerealizedData = false) {
        $additions = "";
        $additionalKeys = array();
        $id = array_search("entry_id", $dataArray[0]);
        if ($id === FALSE)
            return $dataArray;
        $prevEntryID = 0;
        $prevRow = "";

        //###   This could take some time!!!   ###
        foreach ($dataArray as $key => $row) {

            if ($row[$id] === $prevEntryID) {
                //###   Duplicate IDs (therefore shared data), so strip down duplicates   ###
                $updateRow = array_diff($row, $prevRow);
                $updateRow[$id] = $row[$id];
                //###   Delete all the values   ####
                $empties = array_fill(0, count($row), null);
                $row = array_combine(array_keys($row), $empties);
                //###   Add back the different array elements   ###
                foreach ($updateRow as $existingKey => $newValue)
                    $row[$existingKey] = $newValue;

                $dataArray[$key] = $row;
//echo "<pre>PREVrow";var_dump($prevRow);echo "</pre>";
//echo "<pre>row";var_dump($row);echo "</pre>";

            } else {
                $prevEntryID = $row[$id];
                $prevRow = $row;
            }

            if ($hasSerealizedData) {
                $newRows = array();
                foreach ($row as $innerKey => $column) {
                    //###   Test for Serialization   ###
                    $serialData = @unserialize($column);
                    if ($column === 'b:0;' || $serialData !== false) {
                        $unpackedData = unserialize($column);

                        foreach ($unpackedData as $unpackedKey => $unpackedValue) {
                            $newCols = array();
                            //$newCols[$dataArray[0][$id]] = $row[$id];

                            if (is_array($unpackedValue)) {
                                //if (!is_numeric(key($unpackedValue))) {
                                    foreach ($unpackedValue as $individualKey => $individualValue) {
                                        if (is_array($individualValue))
                                            $newCols[$dataArray[0][$innerKey]."_".$individualKey] = implode(",", $individualValue);
                                        else
                                            $newCols[$dataArray[0][$innerKey]."_".$individualKey] = $individualValue;
                                    }
/*
                                if (count($unpackedValue) == 1) {
                                    $newCols[] = array($dataArray[$key]."_".key($unpackedValue) => implode(",", $unpackedValue));


                                if (!is_numeric($unpackedKey)) {
                                    $newCols[] = array($dataArray[$key]."_".$unpackedKey => implode(",", $unpackedValue));
                                } else {
                                    //###   Save Array and remove empty values   ###
                                    $newCols[] = array($dataArray[$key] => array_filter($unpackedValue));
                                } */

                            } else {
                                if (!is_numeric($unpackedKey)) {
                                    $newCols[$dataArray[0][$innerKey]."_".$unpackedKey] = $unpackedValue;
                                } else {
                                    $newCols[$dataArray[0][$innerKey]] = implode(",", $unpackedData);
                                    break;
                                }
                            }


/*$newCols[$id] = $row[$id];
                //###   Delete all the values   ####
                $empties = array_fill(0, count($row), null);
                $tempRow = array_combine(array_keys($row), $empties);
                //###   Add back the different array elements   ###
                foreach ($newCols as $existingKey => $newValue) {
//TO DO - May need to change key look up to look in header line
                    if (array_key_exists($existingKey, $row)) {
                        $tempRow[$existingKey] = $newValue;
                    } else {
                        $tempRow[$existingKey] = $newValue;
//TO DO - Add to top header line
                    }
                }
                //###   Add to existing new row, or create a new one?   ###
                $newRows[] = $tempRow;*/


                            $newRows[] = $newCols;
                        }//###   End of foreach
                    }
                }//###   End of foreach

                //###   Compare and Merge rows without duplicate keys (duplicate keys form new rows)   ###
                if (!empty($newRows)) {
                    $finalRows = array();
                    foreach ($newRows as $entryID => $additionRow) {
                        $rowNum = 0;

                        do {
                            $duplicateCheck = false;
                            if (!empty($finalRows[$rowNum])) {
                                foreach ($additionRow as $checkKey => $newValue) {
                                    if (array_key_exists($checkKey, $finalRows[$rowNum])) {
                                        $duplicateCheck = true;
                                        $rowNum++;
                                        break;
                                    }
                                }//###   End of foreach
                            }
                        } while ($duplicateCheck);

                        if (empty($finalRows[$rowNum])) {
                            $finalRows[$rowNum] = $additionRow;
                        } else {
                            $finalRows[$rowNum] = array_merge($finalRows[$rowNum], $additionRow);
                        }

                        //###   Add any new keys   ###
                        $additionalKeys = array_merge($additionalKeys, array_keys($finalRows[$rowNum]));
                    }//###   End of foreach

                    //###   Add entry ID
                    $additions[$row[$id]] = $finalRows;
                }

            }//###   Serialized Conditional
        }//###   End of foreach

        if (!empty($additions)) {
            //###   Update master column labels (also used as keys)   ###
            $dataArray[0] = array_unique(array_merge($dataArray[0], $additionalKeys));

            //###   Create empty row structure using keys   ####
            $empties = array_fill(0, count($dataArray[0]), null);
            $emptyStructure = array_combine($dataArray[0], $empties);

//echo "<pre>";var_dump($emptyStructure);echo "</pre>";

            foreach ($additions as $entryID => $entryRows) {
                foreach ($entryRows as $newRow) {
                    $newRow["entry_id"] = $entryID;
                    $dataArray[] = array_merge($emptyStructure, $newRow);
                }//###   End of foreach
            }//###   End of foreach



                    /*                if (!empty($newRows)) {
                    array_splice($dataArray, $key, 0, $newRows);
                    //$additions[$row[$id]] = $newRows;
                    $dataArray[$key] = $row;
                }*/


        }

        //###   Re-order Array by entry_id   ###


//echo "<pre>";var_dump($dataArray);echo "</pre>";return;

        return $dataArray;
    }//###   End of clean_export_array function


}//###   END of Class

/* End of file mcp.transporter.php */
/* Location: <third party folder> /transporter/mcp.transporter.php */