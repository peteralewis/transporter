<?php
/*
====================================================================================================
 Author: Peter Lewis
 http://www.peteralewis.com
====================================================================================================
 This file must be in a folder called transporter and placed in the ExpressionEngine installation third_party folder.
 version 		Version 1.0.0
 copyright 		Copyright (c) 2013 Peter Lewis
 Last Update    April 2013
----------------------------------------------------------------------------------------------------
 Purpose:		Provides Export and Import functionality for entries and specifically for Exp:resso Store product data
====================================================================================================
 SEE CP MODULE FILE (mcp.) FOR DESCRIPTION AND CHANGE LOG
====================================================================================================
*/
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', true);

//###   Ensure file is called from within EE   ###
if ( ! defined('EXT')) exit('Invalid file request');

class Transporter {
    var $EE;
    var $site_id;

    //###   PHP5 Constructor   ###
    function __construct() {
        $this->EE =& get_instance();
        $this->site_id = $this->EE->config->item('site_id');

        //###   Manually load the language file if we're accessing from frontend (as not loaded automatically)   ###
        $this->EE->lang->loadfile(__CLASS__);

        //###   Setup the page loading Cache   ###
        if(!isset($this->EE->session->cache)) $this->EE->session->cache = array();
        if(!isset($this->EE->session->cache[__CLASS__])) $this->EE->session->cache[__CLASS__] = array();
    }//###   End of __construct function









}//###   END of Class