<?php if (!defined('EXT')) exit('Invalid file request');
/*
  ====================================================================================================
  Author: Peter Lewis
  http://www.peteralewis.com
  ====================================================================================================
  This file must be in a folder called query_export and placed in the ExpressionEngine installation third_party folder.
  version       Version 1.0.0
  copyright     Copyright (c) 2013 Peter Lewis
  Last Update   February 2013
  ----------------------------------------------------------------------------------------------------
  Purpose:		Provides Export Database functions relevant to Transporter Module
  ====================================================================================================
  SEE CP MODULE FILE (mcp.) FOR DESCRIPTION AND CHANGE LOG
  ====================================================================================================
 */
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', true);

class Export_model extends CI_Model {
    var $class_name = "transporter";
    var $EE, $site_id;

    const IDLE    = 0;
    const STARTED = 1;
    const PROCESS = 2;
    const SUCCESS = 3;
    const ERROR   = 4;

    //###   PHP5 Constructor   ###
    function __construct() {
        $this->EE = & get_instance();
        $this->site_id = $this->EE->config->item('site_id');
    }//###   End of __construct function   ###


    //###   Returns DB object, or false   ###
    function get_entries($channels, $includeMatrix = false, $customTables = "", $customFields = "", $status = "", $limit = 0) {
        $channelSQL = "";
        $output = array();

        //###   Build channels element of SQL   ###
        if (is_array($channels)) {
//TO DO - clean each element of array (escaping and XSS check)
            $channelSQL = " in (".implode(",", $channels).")";
        } else if (!empty($channels)) {
            $channelSQL = " = ".$this->EE->db->escape_str($this->EE->security->xss_clean($channels));
        }

        if (empty($customFields) || !is_array($customFields)) {
            $customFields = $this->filterDefaultFields();
            if ($includeMatrix) {
                $matrixRefs = $this->matrix_references("md");
                $customFields = array_merge($customFields, $matrixRefs);
            }

            if (!empty($customTables) && is_array($customTables)) {
                foreach($customTables as $table) {
                    $sql = "SHOW COLUMNS FROM ".$table;
                    $result = $this->EE->db->query($sql);

                    if ($result->num_rows() > 0) {
                        $result = $result->result_array();
                        foreach($result as $value) {
                            //###   Get column name   ###
                            $field = $value["Field"];
                            $customFields[$field] = $table.".".$field;
                        }
                    }
//TO DO - remove repetitive fields (site_id, channel_id & entry_id)
                }
            }

        } else {
//TO DO - clean each element of array (escaping and XSS check)
        }

/*
		$this->db->select('*, g.group_title as member_group_title');
		$this->db->from('exp_channel_data cd');
        $this->db->from('exp_channel_titles ct');
		$this->db->join('exp_matrix_data md', 'ct.entry_id = md.entry_id');
		$this->db->where('p.promo_code_id' , (int)$promo_code_id);
		$this->db->where('p.site_id', $this->config->item('site_id'));
		$promo_code = $this->db->get()->row_array();
*/

        $sql = "SELECT ";
        foreach ($customFields as $key => $value)
            $sql .= $value." as '".$key."',";
        $sql = substr($sql, 0, -1)." ";
        $sql .= "FROM exp_channel_data cd, exp_channel_titles ct ";
        if ($includeMatrix)
            $sql .= "JOIN exp_matrix_data md on ct.entry_id = md.entry_id ";
        if (!empty($customTables) && is_array($customTables)) {
            foreach($customTables as $table)
                $sql .= "JOIN ".$table." on ct.entry_id = ".$table.".entry_id ";
        }
        $sql .= "WHERE ct.entry_id = cd.entry_id ";
        if (!empty($status)) {
            $status = strtolower($status);
            if (strpos($status, "not ") !== FALSE) {
                $status = str_replace("not ", "", $status);
                if (strpos($status, ","))
                    $status = implode("' and '", explode(",", $status));
                $sql .= "AND ct.status != '".$status."' ";
            } else {
                if (strpos($status, ","))
                    $status = implode("' or '", explode(",", $status));
                $sql .= "AND ct.status = '".$status."' ";
            }
        }
        if (!empty($channelSQL))
            $sql .= "AND ct.channel_id".$channelSQL." ";
        if (!empty($limit))
            $sql .= "LIMIT ".$limit;
//echo "<pre>";var_dump($sql);echo "</pre>";
        $result = $this->EE->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result;
        }

        return false;
    }//###   End of get_entries function   ###


    function filterDefaultFields() {

        $titleTable = "ct";
        $fieldList = array(
            "entry_id"          => $titleTable.".entry_id",
            "channel_id"        => $titleTable.".channel_id",
            "author_id"         => $titleTable.".author_id",
            "title"             => $titleTable.".title",
            "url_title"         => $titleTable.".url_title",
            "status"            => $titleTable.".status",
            "entry_date"        => $titleTable.".entry_date",
            "expiration_date"   => $titleTable.".expiration_date"
        );

        $dataTable = "cd";
        $channelFields = $this->get_channel_data_indices();
        unset($channelFields["entry_id"]);
        unset($channelFields["channel_id"]);
        foreach($channelFields as $key => $value) {
            $fieldList[$value] = $dataTable.".".$key;
        }
//TO DO - expand this to be a multi-dimensional array that holds the "nice name" as well...?

        return $fieldList;
    }//###   End of filterDefaultFields function   ###


/*    function filterDefaultMatrixFields(&$customFields) {

        foreach($matrixRefs as $ref => $name) {

            $customFields[] =
        }
    }//###   End of filterMatrixFields function   ### */


    function get_channel_data_indices() {
        //###   query the db for the tables   ###
        if (isset($this->EE->session->cache[$this->class_name]['db_field_refs']))
            return $this->EE->session->cache[$this->class_name]['db_field_refs'];

        $fieldLabels = array();

        //###   Get Field Titles   ###
        $fieldReferences = $this->field_references();

        if ($fieldReferences) {

            $sql = "SHOW COLUMNS FROM exp_channel_data";
            $result = $this->EE->db->query($sql);

            if ($result->num_rows() > 0) {
                $result = $result->result_array();
                foreach($result as $value) {
                    //###   Replace Field ID reference with name   ###
                    $field = $value["Field"];
                    if (substr($field,0,9) == "field_id_") {
                        $fieldLabels[$field] = $fieldReferences[substr($field,9)];
                    } else if (substr($field,0,9) != "field_ft_") {
                        $fieldLabels[$field] = $field;
                    }
                }//###   End of foreach   ###
            }
        }
        //echo "<pre>";var_dump($fieldLabels);echo "</pre>";

        $this->EE->session->cache[$this->class_name]['db_field_refs'] = $fieldLabels;
        return $fieldLabels;
    }//###   End of entry_indices function   ###




    function get_tables() {
        $tables = $this->get_all_tables();
        $tableList = array();

        foreach($tables as $rawTable) {
            $table = implode("", $rawTable);
            if ($this->check_table_structure($table)) {
                //###   Table has an entry_id column   ###
                $tableList[] = $table;
            }
        }//###   End of foreach   ###

        $excludedTables = array("exp_category_posts",
                                "exp_channel_data",
                                "exp_channel_entries_autosave",
                                "exp_channel_titles",
                                "exp_comment_subscriptions",
                                "exp_entry_ping_status",
                                "exp_entry_versioning",
                                "exp_matrix_data");

        foreach($tableList as $id => $table) {
            if (array_search($table, $excludedTables) !== false)
                unset($tableList[$id]);
        }

        return $tableList;
    }//###   End of get_tables function   ###


    function get_all_tables() {
        //###   query the db for the tables   ###
        if (isset($this->EE->session->cache[$this->class_name]['db_tables'])) {
            return $this->EE->session->cache[$this->class_name]['db_tables'];
        } else {
            $sql = "SHOW TABLES";
            $result = $this->EE->db->query($sql);

            if ($result->num_rows() > 0) {
                $result = $result->result_array();
                $this->EE->session->cache[$this->class_name]['db_tables'] = $result;
                return $result;
            } else {
                return false;
            }
        }
    }//###   End of get_all_tables function   ###


    function check_table_structure($table) {
        //###   query the db for the table structure looking for a column called entry_id   ###
        if (isset($this->EE->session->cache[$this->class_name]['db_valid_table'][$table])) {
            return $this->EE->session->cache[$this->class_name]['db_valid_table'][$table];
        } else {
            //$sql = "DESCRIBE ".$table;
            $sql = "SHOW COLUMNS FROM ".$table." LIKE 'entry_id'";
            $result = $this->EE->db->query($sql);

            if ($result->num_rows() > 0) {
                $this->EE->session->cache[$this->class_name]['db_valid_table'][$table] = true;
                return true;
            } else {
                $this->EE->session->cache[$this->class_name]['db_valid_table'][$table] = false;
                return false;
            }
        }
    }//###   End of check_table_structure function   ###


    function get_channels() {
        //###   query the db for the channels   ###
        if (isset($this->EE->session->cache[$this->class_name]['channels'])) {
            return $this->EE->session->cache[$this->class_name]['channels'];
        } else {
            $sql = "SELECT channel_id, channel_title
                    FROM exp_channels
                    WHERE site_id = ".$this->site_id;
            $result = $this->EE->db->query($sql);

            if ($result->num_rows() > 0) {
                $result = $result->result_array();
                $finalResult = array();
                foreach ($result as $row) {
                    $finalResult[$row["channel_id"]] = $row["channel_title"];
                }

                $this->EE->session->cache[$this->class_name]['channels'] = $finalResult;
                return $finalResult;
            } else {
                return false;
            }
        }
    }//###   End of get_channels function   ###


    function field_references() {
        //###   query the db for the field names   ###
        if (isset($this->EE->session->cache[$this->class_name]['field_references'])) {
            return $this->EE->session->cache[$this->class_name]['field_references'];
        } else {
            $sql = "SELECT field_id, field_name
                    FROM exp_channel_fields
                    WHERE site_id = ".$this->site_id;
            $result = $this->EE->db->query($sql);

            if ($result->num_rows() > 0) {
                $result = $result->result_array();
                $finalResult = array();
                foreach ($result as $row) {
                    $finalResult[$row["field_id"]] = $row["field_name"];
                }

                $this->EE->session->cache[$this->class_name]['field_references'] = $finalResult;
                return $finalResult;
            } else {
                return false;
            }
        }
    }//###   End of field_references function   ###


    function matrix_references($tableName = false) {
        //###   query the db for the matrix columns   ###
        if (isset($this->EE->session->cache[$this->class_name]['matrix_references'])) {
            return $this->EE->session->cache[$this->class_name]['matrix_references'];
        } else {
            $sql = "SELECT col_id, col_name
                      FROM exp_matrix_cols
                     WHERE site_id = ".$this->site_id;
            $result = $this->EE->db->query($sql);

            if ($result->num_rows() > 0) {
                $result = $result->result_array();
                $finalResult = array();
                foreach ($result as $row) {
                    if (!empty($tableName)) {
                        $finalResult[$row["col_name"]] = $tableName.".col_id_".$row["col_id"];
                    } else {
                        $finalResult[$row["col_name"]] = "col_id_".$row["col_id"];
                    }
                }
                $this->EE->session->cache[$this->class_name]['matrix_references'] = $finalResult;
                return $finalResult;
            } else {
                return false;
            }
        }
    }//###   End of matrix_references function   ###


    function cat_references() {
        //###   query the db for the category field references   ###
        if (isset($this->EE->session->cache[$this->class_name]['cat_references'])) {
            return $this->EE->session->cache[$this->class_name]['cat_references'];
        } else {
            $sql = "SELECT field_id id, field_name name, field_label label
                    FROM exp_category_fields
                    WHERE site_id = ".$this->site_id;
            $result = $this->EE->db->query($sql);

            if ($result->num_rows() > 0) {
                $result = $result->result_array();
                $finalResult = array();
                foreach ($result as $row) {
                    $finalResult[$row["id"]] = array( "field_name"  => $row["name"],
                                                      "field_label" => $row["label"]);
                }

                $this->EE->session->cache[$this->class_name]['cat_references'] = $finalResult;
                return $finalResult;
            } else {
                return false;
            }
        }
    }//###   End of cat_references function   ###


    function save_export($saveID = false, $framework, $settings) {
        $data = array();

        $data["site_id"] = $this->site_id;
        if (!empty($framework["name"]))
            $data["name"] = $framework["name"];
        if (!empty($framework["notes"]))
            $data["notes"] = $framework["notes"];
        unset($framework["name"]);
        unset($framework["notes"]);
        $data["framework"] =  serialize($framework);
        $data["settings"] = serialize($settings);

        if ($saveID === false) {
            $data["create_date"] = time();
            $rtnVal = $this->EE->db->insert($this->class_name.'_saves', $data);

            $results = $this->EE->db->query("SELECT MAX(save_id) as id FROM ".$this->EE->db->dbprefix.$this->class_name."_saves");
            if ($results->num_rows() > 0) {
                $results = $results->result_array();
                $saveID = $results[0]["id"];
            }

        } else {
            $this->EE->db->where('save_id', $saveID);
            $rtnVal = $this->EE->db->update($this->class_name.'_saves', $data);
        }

        return $saveID;
    }//###   End of save_export function


    function save_job($saveID, $filename) {
        $jobID = false;

        $data = array(
                "site_id"   =>  $this->site_id,
                "save_id"   =>  $saveID,
                "date"      =>  time(),
                "status"    =>  self::STARTED,
                "path"      =>  $filename
            );
        $rtnVal = $this->EE->db->insert($this->class_name.'_jobs', $data);

        $results = $this->EE->db->query("SELECT MAX(job_id) as id FROM ".$this->EE->db->dbprefix.$this->class_name."_jobs");
        if ($results->num_rows() > 0) {
            $results = $results->result_array();
            $jobID = $results[0]["id"];
        }

        return $jobID;
    }//###   End of save_job function   ###

}//###   End of export Class   ###