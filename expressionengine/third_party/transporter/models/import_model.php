<?php if (!defined('EXT')) exit('Invalid file request');
/*
  ====================================================================================================
  Author: Peter Lewis
  http://www.peteralewis.com
  ====================================================================================================
  This file must be in a folder called query_export and placed in the ExpressionEngine installation third_party folder.
  version       Version 1.0.0
  copyright     Copyright (c) 2013 Peter Lewis
  Last Update   5th March 2013
  ----------------------------------------------------------------------------------------------------
  Purpose:		Provides Import Database functions relevant to Transporter Module
  ====================================================================================================
  SEE CP MODULE FILE (mcp.) FOR DESCRIPTION AND CHANGE LOG
  ====================================================================================================
 */
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', true);

class Import_model extends CI_Model {
    var $class_name = "transporter";
    var $EE, $site_id;
    public $entryIDLookup;

    //###   PHP5 Constructor   ###
    function __construct() {
        $this->EE = & get_instance();
        $this->site_id = $this->EE->config->item('site_id');

        //###   Load EE API   ###
        $this->EE->load->library('api');
        $this->EE->api->instantiate('channel_entries');
        $this->EE->api->instantiate('channel_fields');
    }//###   End of __construct function   ###


    function channel_details() {
        //###   query the db for the channels   ###
        if (isset($this->EE->session->cache[$this->class_name]['channel_details'])) {
            return $this->EE->session->cache[$this->class_name]['channel_details'];
        } else {
            $sql = "SELECT channel_id as id, channel_title as title, field_group as fg, cat_group as cat
                    FROM exp_channels
                    WHERE site_id = ".$this->site_id;
            $result = $this->EE->db->query($sql);

            if ($result->num_rows() > 0) {
                $result = $result->result_array();
                $finalResult = array();
                foreach ($result as $row) {
                    $finalResult[$row["id"]] = array(
                        "title"         => $row["title"],
                        "field_group"   => $row["fg"],
                        "cat_group"     => $row["cat"]
                    );
                }

                $this->EE->session->cache[$this->class_name]['channel_details'] = $finalResult;
                return $finalResult;
            } else {
                return false;
            }
        }
    }//###   End of get_channels function   ###


    function field_details($group = 0) {
        //###   query the db for the field names   ###
        if (isset($this->EE->session->cache[$this->class_name]['field_details'])) {
            return $this->EE->session->cache[$this->class_name]['field_details'];
        } else {
            $sql = "SELECT field_id, field_name, field_label, field_type
                    FROM exp_channel_fields
                    WHERE site_id = ".$this->site_id;
            if (!empty($group))
                $sql .= " AND group_id = ".$group." ";
            $result = $this->EE->db->query($sql);

            if ($result->num_rows() > 0) {
                $result = $result->result_array();
                $finalResult = array();
                foreach ($result as $row) {
                    $finalResult[$row["field_id"]] = array(
                        "name"  => $row["field_name"],
                        "label" => $row["field_label"],
                        "type"  => $row["field_type"]
                    );
                }

                $this->EE->session->cache[$this->class_name]['field_details'] = $finalResult;
                return $finalResult;
            } else {
                return false;
            }
        }
    }//###   End of field_details function   ###


    function add_store_entries($data) {
        $storeMap = $this->store_db_map();
        $modData = array();
        $optionData = array();
        $stockData = array();
        $stockOptionsData = array();
        $optionLookup = array();
        $modifierLookup = array();
//TO DO: Need to identify if this is a replacement row instead of assuming new!

        foreach($data as $entry_id => $newEntry) {
            $coreData = array();
            foreach($newEntry as $rowKey => $row) {
                //if (!empty($row)) { <- not sure if needed!
                $stock = array();
                $modifiers = array();
                $secondary = false;
                if ($row["secondary"] === true)
                    $secondary = true;
                unset($row["secondary"]);

                //###   Core product details (exp_store_products) - a row from import   ###
                foreach($row as $fieldID => $fieldData) {
                    if (!empty($storeMap[$fieldID])) {
                        //###   Standard Fields   ###
                        $coreData[$fieldID] = $fieldData;

                    } else {
                        $variant = explode(":", $fieldID);
                        $fieldData = trim($fieldData);
                        //###   Product Modifiers (non-Store data import) - Search for mapping reference   ###
                        if ($variant[0] == "modifier") {

                            if (empty($fieldData)) {
//TO DO: LOG INVALID DATA

                            } else {
                                $modifiers[$variant[1]] = array(
                                    "entry_id" => $this->entryIDLookup[$entry_id],
                                    "mod_type" => "var",
                                    "mod_name" => $variant[1],
                                    "mod_instructions" => "",
                                    "temp_opt_name" => $fieldData
                                    );

                                $optionData[ $this->entryIDLookup[$entry_id] ][$variant[1]][] = $fieldData;
                            }

                        } else if ($variant[0] == "stock") {
                            $stock[$variant[1]] = $fieldData;
                            if ($secondary)
                                $stock["secondary"] = $secondary;
                        }
                    }
                }//###   End of foreach (Columns)

//TO DO: Need to detect/calculate which type of mod_type is required - default to "var" (values = "var", "var_single_sku" or "text")
//       Check if multiple options and only a single SKU = var_single_sku
//       Check if NO options and only a single SKU = text
//       Amend mod_type of modifiers Array as relevant

                //###   Check we have both details   ###
                if (!empty($stock)) {
                    if (!empty($stock["sku"])) {
                        $stock["entry_id"] = $this->entryIDLookup[$entry_id];
                        if (empty($stock["track_stock"])) {
                            if (!empty($stock["entry_id"]))
                                $stock["track_stock"] = "y";
                            else
                                $stock["track_stock"] = "n";
                        }
                        $stockData[] = $stock;

                        if (!empty($modifiers)) {
                            foreach($modifiers as $modName => $modDetails) {
                                if (empty($modDetails["temp_opt_name"])) {
//TO DO: LOG INVALID DATA

                                } else {
                                    $stockOptionsData[] = array(
                                        "sku"               => $stock["sku"],
                                        "entry_id"          => $stock["entry_id"],
                                        "product_mod_id"    => $modName,
                                        "product_opt_id"    => $modDetails["temp_opt_name"]
                                        );
                                }
                                unset($modifiers[$modName]["temp_opt_name"]);
                            }//###   End of foreach (Modifiers)

                        } else {
//TO DO: Dunno what this will look like in the DB - assume nothing added?!?
                        }
                    }
                }

                if (!empty($modifiers)) {
                    $modData[$this->entryIDLookup[$entry_id]][] = $modifiers;
                }
            }//###   End of foreach (Rows)

            if (!empty($coreData) && !empty($coreData["regular_price"]) && !empty($this->entryIDLookup[$entry_id])) {
                if (empty($coreData["sale_price_enabled"])) {
                    if (!empty($coreData["sale_price"]) && $coreData["sale_price"] != 0.0)
                        $coreData["sale_price_enabled"] = "y";
                    else
                        $coreData["sale_price_enabled"] = "n";
                }
                if (empty($coreData["handling"]))
                    $coreData["handling"] = 0;
                if (empty($coreData["free_shipping"]))
                    $coreData["free_shipping"] = "n";
                if (empty($coreData["tax_exempt"]))
                    $coreData["tax_exempt"] = "n";


                $coreData["entry_id"] = $this->entryIDLookup[$entry_id];
                $rtnVal = $this->EE->db->insert('exp_store_products', $coreData);
            }
        }//###   End of foreach (entry loop)

        if (!empty($modData)) {
            foreach($modData as $entryID => $entryMods) {
                $duplicateCheck = array();
                foreach($entryMods as $order => $modDetails) {
                    foreach($modDetails as $modName => $modFields) {
                        $modFields["mod_order"] = $order;
                        //if (empty($duplicateCheck[$modFields["product_mod_id"]])) {
                        //if (!array_key_exists($modFields["product_mod_id"], $duplicateCheck)) {
                        if (!array_key_exists($modName, $duplicateCheck)) {
                            $rtnVal = $this->EE->db->insert('exp_store_product_modifiers', $modFields);
                            $results = $this->EE->db->query("SELECT MAX(product_mod_id) as id FROM exp_store_product_modifiers");
                            if ($results->num_rows() > 0) {
                                $results = $results->result_array();
                                $modData[$entryID][$order][$modName]["product_mod_id"] = $results[0]["id"];

                                //###  Store in temp array to check for duplicat modifiers (need to still allow for the option though!)   ###
                                //$duplicateCheck[$modFields["product_mod_id"]] = $results[0]["id"];
                                $duplicateCheck[$modName] = $results[0]["id"];

                                //###   Used to re-reference $stockOptionsData   ###
                                $modifierLookup[$entryID][$modName] = $results[0]["id"];

                                //###   Re-index product modifier Options array for ID instead of Modifier name   ###
                                $optionData[$entryID][ $results[0]["id"] ] = $optionData[$entryID][ $modData[$entryID][$order][$modName]["mod_name"] ];
                                unset($optionData[$entryID][ $modData[$entryID][$order][$modName]["mod_name"] ]);
                            }
                        }
                    }//###   End of foreach
                }//###   End of foreach
            }//###   End of foreach
        }

        if (!empty($optionData)) {
            foreach($optionData as $entryID => $entryOptions) {
                $duplicateCheck = array();
                foreach($entryOptions as $mod_id => $optionDetails) {
                    if (!empty($optionDetails)) {
                        foreach($optionDetails as $order => $option) {
                            if (!array_key_exists($mod_id."-".$option, $duplicateCheck)) {
                                $duplicateCheck[$mod_id."-".$option] = $option;
                                $rowData = array(
                                    "product_mod_id" => $mod_id,
                                    "opt_name" => $option,
                                    "opt_order" => $order
                                    );
        //TO DO: add opt_price_mod to above!
                                $rtnVal = $this->EE->db->insert('exp_store_product_options', $rowData);

                                $results = $this->EE->db->query("SELECT MAX(product_opt_id) as id FROM exp_store_product_options");
                                if ($results->num_rows() > 0) {
                                    $results = $results->result_array();

                                    //###   Used to re-reference $stockOptionsData   ###
                                    $optionLookup[$mod_id][$option] = $results[0]["id"];
                                }
                            }
                        }//###   End of foreach
                    }
                }//###   End of foreach
            }//###   End of foreach
        }

        //###   To prevent duplicate entry errors, get current SKUs and maintain check Array   ###
        $skus = array();
        $skuDB = $this->EE->db->query("SELECT sku FROM exp_store_stock");
        if ($skuDB->num_rows() > 0) {
            $emptyArray = array_fill(0, $skuDB->num_rows(), null);
            $skuDB = $skuDB->result_array();
            $skus = array_combine($skuDB, $emptyArray);
        }

        if (!empty($stockData)) {
            foreach($stockData as $row) {
                if (array_key_exists($row["sku"], $skus)) {
//TO DO - LOG DUPLICATE SKU
                } else {
                    $add = true;
                    if (isset($row["secondary"])) {
                        if ($row["secondary"] === true)
                            $add = false;
                        unset($row["secondary"]);
                    }
                    if ($add) {
                        $rtnVal = $this->EE->db->insert('exp_store_stock', $row);
                        $skus[$row["sku"]] = "";
                    }
                }
            }//###   End of foreach
        }

        //###   To prevent duplicate entry errors, get current SKUs and maintain check Array   ###
        $existingIDs = array();
        $idDB = $this->EE->db->query("SELECT CONCAT(sku, '-', product_mod_id) As id FROM exp_store_stock_options");
        if ($idDB->num_rows() > 0) {
            $emptyArray = array_fill(0, $idDB->num_rows(), null);
            $idDB = $idDB->result_array();
            $existingIDs = array_combine($idDB, $emptyArray);
        }

        if (!empty($stockOptionsData)) {
            foreach($stockOptionsData as $row) {
                $modID = $modifierLookup[ $row["entry_id"] ][ $row["product_mod_id"] ];
                //###   Replace option labels with inserted DB ID  ###
                $row["product_opt_id"] = $optionLookup[$modID][ $row["product_opt_id"] ];
                //###   Replace modifier labels with inserted DB ID  ###
                $row["product_mod_id"] = $modID;

                if (empty($row["product_mod_id"]) || empty($row["product_opt_id"])) {
//TO DO - LOG INVALID DATA

                } else {
                    $row["sku"] = trim($row["sku"]);
                    if (array_key_exists($row["sku"]."-".$row["product_mod_id"], $existingIDs)) {
    //TO DO - LOG DUPLICATE SKU
                    } else {
                        $rtnVal = $this->EE->db->insert('exp_store_stock_options', $row);
                        $existingIDs[$row["sku"]."-".$row["product_mod_id"]] = "";
                    }
                }
            }//###   End of foreach
        }
//echo "<pre>";var_dump($existingIDs);echo "</pre>";


//TO DO - consider automatically deleting any previous references to entry_id and re-insert?




    }//###   End of add_store_entries function   ###


    function add_matrix_entries($data, $updateLookup) {

//TO DO: Need to identify if this is a replacement row instead of assuming new!

        foreach($data as $entry_id => $newEntry) {
            if (!empty($this->entryIDLookup[$entry_id])) {
                foreach($newEntry as $rowKey => $row) {
                    foreach($row as $fieldID => $fieldData) {
                        $newData = array(
                                    'site_id' => $this->site_id,
                                    'entry_id' => $this->entryIDLookup[$entry_id],
                                    'field_id' => substr($fieldID, 9),//key($fieldData),
                                    'row_order' => $rowKey+1
                                );
                        foreach($fieldData as $colKey => $column) {
                            $newData[$colKey] = $column;
                        }//###   End of foreach

                        $rtnVal = $this->EE->db->insert('exp_matrix_data', $newData);
                    }//###   End of foreach
                }//###   End of foreach

                //###   Update existing entry (that has been added in this import)   ###
                $updateData = array(
                        $fieldID => "1"
                    );
                $this->EE->db->where('entry_id', $entry_id);
                $rtnVal = $this->EE->db->update('channel_data', $updateData);
            }
        }//###   End of foreach
    }//###   End of add_matrix_entries function   ###


    function add_playa_entries($data, $updateLookup) {
        //###   Load EE API   ###
/*        $this->EE->load->library('api');
        $this->EE->api->instantiate('channel_entries');
        $this->EE->api->instantiate('channel_fields'); */

//TO DO: Need to identify if this is a replacement row instead of assuming new!

        foreach($data as $entry_id => $newEntry) {
            $importEntryData = array();
            foreach($newEntry as $rowKey => $row) {
//TO DO: Could assume always single array element...?
                foreach($row as $fieldID => $fieldData) {

                    //$fieldData - need to get on a line by line basis getting initial reference (child_entry_id)
                    $fieldData = preg_split("/((\r?\n)|(\r\n?))/", $fieldData);
                    foreach($fieldData as $line){
                        // do stuff with $line
                        $relatedEntry = explode("] ", substr($line, 1));

                        if ( isset($this->entryIDLookup[$entry_id]) && isset($this->entryIDLookup[$relatedEntry[0]]) ) {
                            $newData = array(
                                        'parent_entry_id' => $this->entryIDLookup[$entry_id],
                                        'parent_field_id' => substr($fieldID, 9),
                                        'child_entry_id' => $this->entryIDLookup[$relatedEntry[0]]
                                    );
                            $rtnVal = $this->EE->db->insert('exp_playa_relationships', $newData);
                        }

                        if (isset($this->entryIDLookup[$relatedEntry[0]])) {
                            $importEntryData[] = "[".$this->entryIDLookup[$relatedEntry[0]]."] ".$relatedEntry[1];
                        }
                    }
                }//###   End of foreach
            }//###   End of foreach

            //###   Now add combined details to DB...
/*            $importFields = array(
                $fieldID => implode("/n", $importEntryData),
                "channel_id" => $updateLookup[$this->entryIDLookup[$entry_id]]["channel_id"],
                "title"         => $updateLookup[$this->entryIDLookup[$entry_id]]["title"]
            );*/
            //###   Update existing entry (that has been added in this import)   ###
            $updateData = array(
                    $fieldID => implode("/n", $importEntryData)
                );
            $this->EE->db->where('entry_id', $entry_id);
            $rtnVal = $this->EE->db->update('channel_data', $updateData);

                    //$this->EE->api_channel_fields->setup_entry_settings($channelID, $importFields);
/*
echo "<pre>";var_dump($importFields);echo "</pre>";
                    if ($this->EE->api_channel_entries->update_entry($this->entryIDLookup[$entry_id], $importFields) === FALSE) {
                        //###   ERROR!   ###
echo "<pre>";var_dump("EE Error");echo "</pre>";
//EXAMPLE:
/*array(2) {
  ["title"]=>
  string(13) "missing_title"
  ["url_title"]=>
  string(26) "unable_to_create_url_title"
}*/
//echo "<pre>";var_dump($this->EE->api_channel_entries->errors);echo "</pre>";
//ERROR - Unable to EDIT via API
//                    }




        }//###   End of foreach
    }//###   End of add_playa_entries function   ###


    private function store_db_map() {
        return array(
            "entry_id" => "exp_store_products",
            "regular_price" => "exp_store_products",
            "sale_price" => "exp_store_products",
            "sale_price_enabled" => "exp_store_products",
            "sale_start_date" => "exp_store_products",
            "sale_end_date" => "exp_store_products",
            "weight" => "exp_store_products",
            "dimension_l" => "exp_store_products",
            "dimension_w" => "exp_store_products",
            "dimension_h" => "exp_store_products",
            "handling" => "exp_store_products",
            "free_shipping" => "exp_store_products",
            "tax_exempt" => "exp_store_products"
            );
/*
exp_store_product_modifiers
        exp_store_product_options (auto increment)
        entry_id
        mod_type (values = "var", "var_single_sku" or "text")
        mod_name
        mod_instructions
        mod_order

exp_store_product_options
        product_opt_id (auto increment)
        product_mod_id
        opt_name
        opt_price_mod
        opt_order

exp_store_stock
        "sku"
        "entry_id"
        "stock_level"
        "min_order_qty"
        "track_stock"

exp_store_stock_options
        "sku"
        "entry_id"
        "product_mod_id"
        "product_opt_id"
 */

    }//###   End fo store_db_map function   ###


    //###   Duplicate from Import Library!   ###
    function recursive_array_search($needle, $haystack) {
        if (!is_array($haystack))
            return false;

        foreach ($haystack as $key => $value) {
            $current_key = $key;
            if ($needle === $value OR (is_array($value) && $this->recursive_array_search($needle, $value) !== false)) {
                return $current_key;
            }
        }
        return false;
    }//###   End of recursive_array_search function

}//###   End of import Model Class   ###