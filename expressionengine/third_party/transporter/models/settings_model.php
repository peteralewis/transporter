<?php if ( ! defined('EXT')) exit('Invalid file request');

class Settings_model {
    var $EE, $site_id;
    var $class_name = "transporter";

    //###   PHP5 Constructor   ###
    function __construct() {
        $this->EE =& get_instance();
        $this->site_id = $this->EE->config->item('site_id');
    }//###   End of __construct function


    function get() {
        $sql = "SELECT setting, value FROM ".$this->EE->db->dbprefix . $this->class_name ."_settings
		WHERE site_id = ".$this->site_id;
        $result = $this->EE->db->query($sql);
        $settings = false;

        if ($result->num_rows() != 0) {
            $result = $result->result_array();

            $settings = array();
            foreach($result as $details)
                $settings[$details['setting']] = $details['value'];
        }

        return $this->defaults($settings);
    }//###   End of get (settings) function   ###


    function set() {
/* EXAMPLE validation */

        if (isset($_POST['product_channel']))
            $_POST['product_channel'] = preg_replace("/[^0-9]/", '', $this->EE->input->post('product_channel'));
/* END EXAMPLE */
        //###   Remove any additional POST strings   ###
        foreach($_POST as $key => $value) {
            if (!empty($value)) {
                if (is_array($value)) {
                    //###   Remove the individual values passed via post (using array variable instead)   ###
                    foreach ($value as $individualKey => $IndividualValue)
                        unset($_POST[$key.'_' . $individualKey]);
                }
            }
        }//###   End of foreach

        unset($_POST['submit']);

        //###   Delete all current settings out of database - makes it easier to insert/update   ###
        $sql = "DELETE FROM ".$this->EE->db->dbprefix .$this->class_name."_settings
                 WHERE site_id = ".$this->site_id;
        $this->EE->db->query($sql);

        //###   Insert new settings into Database   ###
        foreach($_POST as $key => $value) {
            if (!empty($value)) {
                //###   Serialize any Arrays to store in Database   ###
                if (is_array($value))
                    $value = serialize($value);

                //###   Clean Keys   ###
                $key = $this->EE->db->escape_str($key);
                $key = $this->EE->security->xss_clean($key);

                //###   Clean Values   ###
                $value = $this->EE->db->escape_str($value);
                $value = $this->EE->security->xss_clean($value);

                $sql = $this->EE->db->insert_string(
                                        $this->EE->db->dbprefix . $this->class_name."_settings",
                                        array(
                                            'setting'   => $key,
                                            'value'     => $value,
                                            'site_id'   => $this->site_id
                                        ));
                $this->EE->db->query($sql);
            }
        } //###   End of foreach

        return true;
    }//###   End of set (settings) function   ###


    private function defaults($settings = "") {
// EXAMPLES
        if (empty($settings['email_name']))
            $settings['email_name'] = $this->EE->config->item('site_name');

        if (empty($settings['email_address']))
            $settings['email_address'] = $this->EE->config->item('webmaster_email');

        if (empty($settings['email_title']))
            $settings['email_title'] = lang('email_alert_title');

        if (empty($settings['product_channel']))
            $settings['product_channel'] = 1;
// END EXAMPLES
        return $settings;
    }//###   End of defaults (settings) function   ###





}//###   END of Class