<?php if ( !defined('BASEPATH')) exit('No direct script access allowed');

/*
====================================================================================================
 Author: Peter Lewis
 http://www.peteralewis.com
====================================================================================================
 This file must be in a folder called transporter and placed in the ExpressionEngine installation third_party folder.
 version 		Version 1.0.0
 copyright 		Copyright (c) 2013 Peter Lewis
 Last Update    April 2013
----------------------------------------------------------------------------------------------------
 SEE CP MODULE FILE (mcp.) FOR DESCRIPTION AND CHANGE LOG
====================================================================================================
*/

class Transporter_upd {

    var $version = '1.0.0';
    var $class_name = 'Transporter';
    var $class_nice_name;

    //###   PHP5 Constructor   ###
    function __construct() {
        $this->EE =& get_instance();
        $this->class_nice_name = strtolower($this->class_name);
    }//###   End of __construct function


    //###   Module installation   ###
    function install() {

        //###   Add module to EE modules list   ###
        $data = array(
                'module_name' => $this->class_name,
                'module_version' => $this->version,
                'has_cp_backend' => 'y',
                'has_publish_fields' => 'n'
        );
        $this->EE->db->insert('modules', $data);

        //###   Create table to store the Module settings   ###
        $this->EE->db->query("CREATE TABLE IF NOT EXISTS `".$this->EE->db->dbprefix . $this->class_nice_name ."_settings` (
                                `id`		INT(8)          NOT NULL	AUTO_INCREMENT,
                                `site_id`	INT(8)          NOT NULL	DEFAULT '1',
                                `setting`	VARCHAR(100)	NULL,
                                `value`		TEXT            NULL,
                                PRIMARY KEY (`id`))
                                ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        //###   Create table to store the exports/imports   ###
        $this->EE->db->query("CREATE TABLE IF NOT EXISTS `".$this->EE->db->dbprefix . $this->class_nice_name ."_saves` (
                                `save_id`       INT(8)          NOT NULL	AUTO_INCREMENT,
                                `site_id`       INT(8)          NOT NULL	DEFAULT '1',
                                `name`          VARCHAR(150)	NULL,
                                `notes`         TEXT        	NULL,
                                `create_date`	INT(10)         NULL,
                                `run_date`      INT(10)         NULL,
                                `settings`      TEXT            NULL,
                                `framework`     TEXT            NULL,
                                PRIMARY KEY (`save_id`))
                                ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        $this->EE->db->query("CREATE TABLE IF NOT EXISTS `".$this->EE->db->dbprefix . $this->class_nice_name ."_jobs` (
                                `job_id`        INT(8)          NOT NULL	AUTO_INCREMENT,
                                `site_id`       INT(8)          NOT NULL	DEFAULT '1',
                                `save_id`       INT(8)      	NULL,
                                `date`          INT(10)         NULL,
                                `status`        INT(2)          NULL,
                                `progress`      INT(8)          NULL,
                                `total`         INT(8)          NULL,
                                `path`          VARCHAR(200)    NULL,
                                `errors`        VARCHAR(200)    NULL,
                                PRIMARY KEY (`job_id`))
                                ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        return TRUE;
    }//###   End of install function


    //###   Module uninstall   ###
    function uninstall() {
        $this->EE->db->select('module_id');
        $query = $this->EE->db->get_where('modules', array('module_name' => $this->class_name));

        //###   Remove any member group access to the module   ###
        $this->EE->db->where('module_id', $query->row('module_id'));
        $this->EE->db->delete('module_member_groups');

        //###   Remove the module from EE list   ###
        $this->EE->db->where('module_name', $this->class_name);
        $this->EE->db->delete('modules');

        $this->EE->load->dbforge();
        $this->EE->dbforge->drop_table($this->class_nice_name .'_settings');
        $this->EE->dbforge->drop_table($this->class_nice_name .'_saves');
        $this->EE->dbforge->drop_table($this->class_nice_name .'_jobs');

        return TRUE;
    }//###   End of uninstall function


    //###   Function to update the module (run when Modules screen is visited   ###
    function update($current='') {
        if ($current == '' OR $current == $this->version)
            return FALSE;

        return TRUE;
    }//###   End of update function

}//###   END of Class