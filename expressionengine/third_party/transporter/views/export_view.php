<?php if ( !defined('BASEPATH')) exit('No direct script access allowed'); ?>

    <div id="transporter-export" class="module-cp clear-after">

        <form method="post" action="<?= $submitURL; ?>">
			<input type="hidden" name="XID" value="<?= $XID; ?>" />

            <p><?= lang('label_channels'); ?></p>
            <ul id="channel-list">
<?php       foreach($channels as $id => $row) : 	//###   Loop through each row of data   ### ?>
                <li>
                    <label>
                        <input type="checkbox" name="channels[]" value="<?= $id; ?>" />
                        <?= $row; ?>
                    </label>
                </li>
<?php       endforeach; ?>
            </ul>

            <p><?= lang('label_tables'); ?></p>
            <ul id="db-table-list">
<?php       foreach($tables as $row) : 	//###   Loop through each row of data   ### ?>
                <li>
                    <label>
                        <input type="checkbox" name="tables[]" value="<?= $row; ?>" />
                        <?= $row; ?>
                    </label>
                </li>
<?php       endforeach; ?>
            </ul>

            <ul id="other-options">
                <li>
                    <label>
                        <input type="checkbox" name="matrix" value="yes" />
                        <?= lang('label_include_matrix'); ?>
                    </label>
                </li>
                <li>
                    <p><?= lang('label_export_format'); ?></p>
                    <ul>
                        <li>
                            <label>
                                <input type="radio" name="format" value="csv" />
                                <?= lang('option_export_format_csv'); ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="format" value="xml" />
                                <?= lang('option_export_format_xml'); ?>
                            </label>
                        </li>
                    </ul>
                </li>
                <li>
                    <p><?= lang('label_filename'); ?></p>
                    <input type="text" name="filename" />
                </li>
                <li>
                    <p><?= lang('label_delimiter'); ?></p>
                    <input type="text" name="delimiter" value="<?= $defaultDelimiter; ?>" />
                </li>
<!--                <li>
                    <p><?= lang('label_newline'); ?></p>
                    <input type="text" name="newline" value="<?= htmlentities($defaultNewline); ?>" />
                </li> -->
            </ul>

			<div class="controls">
				<input type="submit" name="submit" class="submit" value="<?= lang('export_run_butn') ?>" />
			</div>
		</form>

	</div><!-- End of #transporter-export -->