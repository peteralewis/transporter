<?php if ( !defined('BASEPATH')) exit('No direct script access allowed'); ?>

    <div id="transporter-import" class="module-cp clear-after">


        <form method="post" action="<?= $submitURL; ?>" enctype="multipart/form-data" >
			<input type="hidden" name="XID" value="<?= $XID; ?>" />

            <h3>Select CSV Import File</h3>
            <ul class="upload">
                <li>
                    <label>File
                        <input type="file" name="<?= $uploadField; ?>" />
                    </label>
                </li>
            </ul>

			<div class="controls">
				<input type="submit" class="submit" value="Import File" />
			</div>
		</form>

	</div><!-- End of #transporter-export -->