<?php if ( !defined('BASEPATH')) exit('No direct script access allowed'); ?>

    <div id="transporter-import" class="module-cp clear-after">


        <form method="post" action="<?= $submitURL; ?>" enctype="multipart/form-data" >
			<input type="hidden" name="XID" value="<?= $XID; ?>" />

            <h3>Select CSV Import File</h3>
            <ul class="upload">
                <li>
                    <label>
                        <input type="checkbox" name="channels[]" value="<?= $id; ?>" />
                        <input type="file" name="<?= $uploadField; ?>" />
                        <?= $row; ?>
                    </label>
                </li>
            </ul>

			<div class="controls">
				<input type="submit" name="submit" class="submit" value="Import File" />
			</div>
		</form>

	</div><!-- End of #transporter-export -->