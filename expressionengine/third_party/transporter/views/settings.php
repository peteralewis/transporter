<?php if ( !defined('BASEPATH')) exit('No direct script access allowed'); ?>
	<div id="module-settings">

            <form method="post" action="<?= $submitURL; ?>">
                <input type="hidden" name="XID" value="<?= $XID; ?>" />
<!--                <p class="instructions"><?= lang('settings_desc'); ?></p> -->
                <ul>
<?php               foreach ($settings as $key => $value) : ?>
                        <li>
                            <label><?= lang($key); ?></label>
<?php                       if ($key == "example_textarea") : ?>
                                <textarea name="<?= $key; ?>"><?= $value; ?></textarea>
<?php                       elseif ($key == "example_dropdown") : ?>
                                <select name="<?= $key; ?>">
<?php                               foreach ($loopData as $row) : ?>                                    
                                        <option<?= ($row == $value) ? ' selected="selected"' : '' ?>><?= $row; ?></option>
<?php                               endforeach; ?>
                                </select>
<?php                       else : ?>
                                <input type="text" name="<?= $key; ?>" value="<?= $value; ?>" />
<?php                       endif ?>
                        </li>
<?php               endforeach; ?>
                </ul>
			
                <div class="controls">
                    <input type="submit" name="submit" class="submit" value="<?= lang('settings_save') ?>" />
                </div>
            </form>
	</div>